const express = require("express")
// Khởi tạo app nodejs bằng express
const app = new express()
// Khai báo ứng dụng chạy trên cổng 8000
const port = 8001
// Khai báo Api trả về chuỗi
app.get("/", (req, res) => {
    var drink = `drink drink`
    res.send(drink)
})
app.listen(port, () => {
    console.log("chạy app")
})
// Tạo class
class Drink {
    constructor(paramId, paramMaNuocUong, paramTenNuocUong, paramDonGia) {
        this.id = paramId
        this.maNuocUong = paramMaNuocUong
        this.TenNuocUong = paramTenNuocUong
        this.donGia = paramDonGia
        this.ngayTao = "14/5/2021"
        this.ngayCapNhat = "14/5/2021"
    }
}
app.get("/1", (req, res) => {
    var traTac = new Drink(1, "TRATAC", "Trà tắc", 10000)
    res.json({
        traTac
    })
})
app.get("/2", (req, res) => {
    var coca = new Drink(1, "COCA", "Cocacola", 15000)
    res.json({
        coca
    })
})
app.get("/3", (req, res) => {
    var pepsi = new Drink(1, "PEPSI", "Pepsi", 15000)
    res.json({
        pepsi
    })
})

app.get("/tratac", (req, res) => {
    var traTac={id:1, maNuocUong:"TRATAC",TenNuocUong: "Trà tắc",donGia: 10000}
    res.json({
        traTac
    })
})
app.get("/coca", (req, res) => {
    var coca={id:2, maNuocUong:"COCA",TenNuocUong: "Cocacola",donGia: 10000}
    res.json({
        coca
    })
})
app.get("/pepsi", (req, res) => {
    var pepsi={id:3, maNuocUong:"PEPSI",TenNuocUong: "Pepsi",donGia: 10000}
    res.json({
        pepsi
    })
})